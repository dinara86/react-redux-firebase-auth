import {applyMiddleware, createStore} from "redux";
import {logger} from "redux-logger";
import rootReducer from "./reducers/rootReducer";
import {composeWithDevTools} from "redux-devtools-extension";
import {loginAction} from "./actions/action";
import thunk from "redux-thunk";
import Cookies from 'js-cookie';

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
}

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middlewares)));

const token = Cookies.getJSON('token');

if (token) {
    store.dispatch(loginAction(token));
}

export default store;

//using redux-toolkit

// import {configureStore} from "@reduxjs/toolkit";
// import userReducer from "../features/userSlice";
//
// export default configureStore({
//     reducer: {
//         user: userReducer
//     }
// })

