import produce from "immer";
import * as types from '../types';

const initialState = {
    user: null,
    error: null,
    loading: false
};

const reducer = produce((draft, action) => {
    // eslint-disable-next-line default-case
    switch (action.type) {
        case types.LOGIN:
            draft.user = action.payload;
            draft.loading = false;
            break;
        case types.REGISTER:
            draft.user = action.payload;
            draft.loading = false;
            break;
        case types.LOGOUT:
            draft.user = null;
            draft.loading = false;
            break;
        case types.SET_LOADING:
            draft.loading = true;
            break;
        case types.SET_ERROR:
            draft.error = action.payload;
            draft.loading = false;
            break;
    }
}, initialState);

export default reducer;