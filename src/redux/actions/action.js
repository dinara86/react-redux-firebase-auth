import * as types from '../types';
import firebaseApp from "../../firebase/firebase.app";
import firebase from "firebase";
import Cookies from 'js-cookie';

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const hour = new Date(new Date().getTime() + 60 * 60 * 1000);

export const loginWithGoogle = () => async dispatch => {
    try {
        const res = await firebaseApp.auth().signInWithPopup(googleAuthProvider);
        const user = await res.user;
        const generalUserData = {
            email: user.email,
            displayName: user.displayName,
            picture: user.photoURL,
            uuid: user.uid
        };

        Cookies.set('token', JSON.stringify(generalUserData), { expires: hour });
        dispatch(loginAction(generalUserData));
    } catch (e) {
        console.log(e)
    }
};

export const login = ({ email, password }) => async dispatch => {
    dispatch(setLoading());

    try {
        const res = await firebaseApp.auth().signInWithEmailAndPassword(email, password);
        const user = await res.user;
        const generalUserData = {
            email: user.email,
            displayName: user.displayName,
            picture: user.photoURL,
            uuid: user.uid
        };

        Cookies.set('token', JSON.stringify(generalUserData), { expires: hour } );
        dispatch(loginAction(generalUserData));
    } catch (error) {
        // const handleError = () => {
        //     switch (error.code) {
        //         case "auth/invalid-email":
        //             return "Invalid email address format.";
        //         case "auth/user-not-found":
        //         case "auth/wrong-password":
        //             return "Invalid email address or password.";
        //         case "auth/too-many-requests":
        //             return "Too many request. Try again in a minute.";
        //         default:
        //             return "Check your internet connection.";
        //     }
        // };

        dispatch(setError(error.message));
    }
};

export const register = ({ name, email, password }) => async dispatch => {
    dispatch(setLoading());

    try {
        const res =  await firebaseApp.auth().createUserWithEmailAndPassword(email, password);
        await firebaseApp.auth().currentUser.sendEmailVerification();
        await firebaseApp.auth().currentUser.updateProfile({
            displayName: name
        });
        const user = await res.user;

        const generalUserData = {
            email: user.email,
            displayName: user.displayName,
            uuid: user.uid
        };

        Cookies.set('token', JSON.stringify(generalUserData), { expires: hour });
        dispatch(registerAction(generalUserData));
    } catch (error) {
        // const handleError = () => {
        //     switch (error.code) {
        //         case "auth/email-already-in-use":
        //             return "E-mail already in use.";
        //         case "auth/invalid-email":
        //             return "Invalid e-mail address format.";
        //         case "auth/weak-password":
        //             return "Password is too weak.";
        //         case "auth/too-many-requests":
        //             return "Too many request. Try again in a minute.";
        //         default:
        //             return "Check your internet connection.";
        //     }
        // };

        dispatch(setError(error.message));
    }
};

export const logout = () => dispatch => {
    dispatch(setLoading());
    firebaseApp.auth().signOut();
    Cookies.remove('token');
    dispatch(logoutAction());
};

export const loginAction = (data) => ({
    type: types.LOGIN,
    payload: data
});

const registerAction = (data) => ({
    type: types.REGISTER,
    payload: data
});

const logoutAction = () => ({
   type: types.LOGOUT
});

const setLoading = () => ({
    type: types.SET_LOADING
});

export const setError = (error) => ({
    type: types.SET_ERROR,
    payload: error
});