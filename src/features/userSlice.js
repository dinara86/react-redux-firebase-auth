import {createSlice} from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",   //nazvanie reducer-a
    initialState: {
        user: null
    },
    reducers: {    //login - eto obrabotchik (like case ..)
        login: (state, action) => {
            state.user = action.payload;
        },
        logout: (state) => {
            state.user = null;
        }
    }
});

export const {login, logout} = userSlice.actions;   //login - eto action

export const selectUser = (state) => state.user.user;   //eto dla useSelector

export default userSlice.reducer;