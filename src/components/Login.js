import React from 'react';
import {login} from "../redux/actions/action";
import {useDispatch} from "react-redux";
import Form from "./Form";
import InputPassword from "./InputPassword";
import InputText from "./InputText";

const Register = props => {
    const dispatch = useDispatch();
    const [form, setForm] = React.useState({
        email: "",
        password: ""
    });

    function handleChange(event) {
        const {name, value} = event.target;

        setForm(_form => ({
            ..._form,
            [name]: value
        }));

    }

    function handleSubmit() {
        dispatch(login(form));
    }

    return (
        <Form title='Login' handleSubmit={handleSubmit}>
            <InputText
                required
                value={form.email}
                label="Email"
                type="email"
                name="email"
                onChange={handleChange}
            />
            <InputPassword
                required
                value={form.password}
                onChange={handleChange}
            />
        </Form>
    )
};

export default Register;