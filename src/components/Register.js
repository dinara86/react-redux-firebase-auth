import React from 'react';
import {register} from "../redux/actions/action";
import {useDispatch} from "react-redux";
import Form from "./Form";
// import {makeStyles} from "@material-ui/core";
import InputPassword from "./InputPassword";
import InputText from "./InputText";

// const useStyles = makeStyles(theme => ({
//     root: {
//         textAlign: 'left',
//         width: '100%',
//         marginBottom: 16
//     },
//     input: {
//         display: 'none'
//     }
// }));

const Register = props => {
    // const classes = useStyles();
    const dispatch = useDispatch();
    const [form, setForm] = React.useState({
        email: "",
        password: "",
        name: "",
        // picture: '',
        // pictureName: ''
    });

    function handleChange(event) {
        const {name, value} = event.target;

        setForm(_form => ({
            ..._form,
            [name]: value
        }));

    }

    // function handleImageChange(e) {
    //     e.preventDefault();
    //     let file = e.target.files[0];
    //     let reader = new FileReader();
    //     reader.readAsDataURL(file);
    //     reader.onloadend = () => {
    //         setForm(_form => ({
    //             ..._form,
    //             pictureName: file.name,
    //             picture: reader.result
    //         }))
    //     }
    // }

    function handleSubmit() {
        dispatch(register(form));
    }

    return (
        <Form title='Register' handleSubmit={handleSubmit} disabled={form.name.length < 3}>
            <InputText
                value={form.name}
                required
                label="Name"
                name="name"
                onChange={handleChange}
            />
            <InputText
                value={form.email}
                label="Email"
                required
                type="email"
                name="email"
                onChange={handleChange}
            />
            <InputPassword
                required
                value={form.password}
                onChange={handleChange}
            />

            {/*<div className={classes.root}>*/}
            {/*    <input*/}
            {/*        accept="image/*"*/}
            {/*        className={classes.input}*/}
            {/*        onChange={handleImageChange}*/}
            {/*        id="icon-button-file"*/}
            {/*        type="file"*/}
            {/*    />*/}
            {/*    <label htmlFor="icon-button-file">*/}
            {/*        <IconButton color="primary" aria-label="upload picture" component="span">*/}
            {/*            <PhotoCamera />*/}
            {/*        </IconButton>*/}
            {/*    </label>*/}
            {/*    <strong style={{ marginLeft: 8 }}>{form.pictureName}</strong>*/}
            {/*</div>*/}
        </Form>
    )
};

export default Register;