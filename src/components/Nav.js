import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from "@material-ui/core/Button";
import {NavLink, useHistory} from "react-router-dom";
import {logout} from "../redux/actions/action";
import {useDispatch, useSelector} from "react-redux";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between'
    }
}));

export default function Nav() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.reducer.user);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    let history = useHistory();

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.toolbar}>
                    <Button exact component={NavLink} to="/" color="inherit">
                        Home
                    </Button>
                    {user ? (
                        <div>
                            <IconButton
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                {user.picture ? (
                                    <Avatar src={user.picture} />
                                ) : (
                                    <Avatar>
                                        {user.displayName[0]}
                                    </Avatar>
                                )}

                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={() => {
                                    history.push('/profile');
                                    handleClose();
                                }}>
                                    Profile
                                </MenuItem>
                                <MenuItem onClick={() => {
                                    dispatch(logout());
                                    history.push('/login');
                                    handleClose();
                                }}>
                                    Log out
                                </MenuItem>
                            </Menu>
                        </div>
                    ) : (
                        <div>
                            <Button exact component={NavLink} to="/login" color="inherit">Login</Button>
                            <Button exact component={NavLink} to="/register" color="inherit">Register</Button>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}