import React from 'react';
import TextField from "@material-ui/core/TextField";

const InputText = props => {
    return (
        <TextField
            {...props}
            style={{ marginBottom: 16 }}
            fullWidth
            variant="outlined"
        />
    );
};

export default InputText;