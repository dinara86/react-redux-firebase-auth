import React from "react";
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";

export function UserOnline({ children, ...rest }) {
    const user = useSelector(state => state.reducer.user);

    return (
        <Route
            {...rest}
            render={({ location }) =>
                user ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}

export function UserOffline({ children, ...rest }) {
    const user = useSelector(state => state.reducer.user);

    return (
        <Route
            {...rest}
            render={({ location }) =>
                !user ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/",
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}
