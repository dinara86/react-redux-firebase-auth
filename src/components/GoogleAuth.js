import React from 'react';
import Button from "@material-ui/core/Button";
import {loginWithGoogle} from "../redux/actions/action";
import {useDispatch} from "react-redux";

function GoogleIcon(props) {
    return <i className="fab fa-google"></i>
}

const GoogleAuth = () => {
    const dispatch = useDispatch();

    const handleGoogleAuthorization = () => {
        dispatch(loginWithGoogle())
    };

    return (
        <Button
            variant="contained"
            color="secondary"
            onClick={handleGoogleAuthorization}
            startIcon={<GoogleIcon />}
        >
            Google
        </Button>
    );
};

export default GoogleAuth;