import React from 'react';
import {useSelector} from "react-redux";

const Home = props => {
    const user = useSelector(state => state.reducer.user);

    return (
        <div>
            {user ? (
                <h2>Welcome {user.displayName}</h2>
            ) : (
                <h2>You are not logged in.</h2>
            )}
        </div>
    )
};

export default Home;