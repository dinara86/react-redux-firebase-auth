import React from 'react';
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {setError} from "../redux/actions/action";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import GoogleAuth from "./GoogleAuth";

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 662,
        margin: '0 auto',
        height: 'calc(100vh - 64px)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: 400,
        minWidth: 320,
        padding: theme.spacing(2)
    },
    wrapper: {
        position: 'relative',
        marginBottom: theme.spacing(2)
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12
    },
    text: {
        textAlign: 'center',
        marginBottom: theme.spacing(2)
    },
    socialButtonWrapper: {
        borderTop: `1px solid ${theme.palette.divider}`,
        paddingBottom: 0
    }
}));

const Form = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading, error } = useSelector(state => state.reducer);

    React.useEffect(() => {
        return () => {
            dispatch(setError(null));
        }
    }, [dispatch]);

    return (
        <div className={classes.root}>
            <Paper elevation={1} className={classes.paper}>
                <Typography variant="h4" className={classes.text}>{props.title}</Typography>

                {props.children}

                {error && <Typography color="secondary" className={classes.text}>{error}</Typography>}

                <div className={classes.wrapper}>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={loading || props.disabled}
                        onClick={props.handleSubmit}
                    >
                        {props.title}
                    </Button>
                    {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                </div>

                <div className={[classes.socialButtonWrapper, classes.paper].join(' ')}>
                    <GoogleAuth />
                </div>
            </Paper>
        </div>
    );
};

export default Form;