import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Nav from "./components/Nav";
import Register from "./components/Register";
import Login from "./components/Login";
import Profile from "./components/Profile";
import {UserOffline, UserOnline} from "./components/PrivateRoute";
import Home from "./components/Home";

export default function AuthExample() {
    return (
        <Router>
            <div>
                <Nav />

                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <UserOffline exact path="/login">
                        <Login />
                    </UserOffline>
                    <UserOffline exact path="/register">
                        <Register />
                    </UserOffline>
                    <UserOnline exact path="/profile">
                        <Profile />
                    </UserOnline>
                    <Route exact path="*">
                        <div>
                            Error
                        </div>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

